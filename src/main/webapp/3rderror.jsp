<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="model.JavaBeans" %>
<%@ page import="model.Espec" %>
<%@ page import="model.Plano" %>
<%@ page import="java.util.ArrayList" %>
<%
	ArrayList<JavaBeans> consultas = (ArrayList<JavaBeans>) request.getAttribute("consultasAgendadas");
	ArrayList<Espec> especialidades = (ArrayList<Espec>) request.getAttribute("especsCadastradas");
	ArrayList<Plano> planos = (ArrayList<Plano>) request.getAttribute("planosCadastradas");
%>
<!DOCTYPE html>
<html lang = "pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ocorreu um erro</title>
        <link rel="stylesheet" href="style.css">
        <style>
            fieldset{
                border-radius: 10px;
                border: 1px solid #DB4437;
            }
            h1{
            	color: #DB4437;
            }


        </style>
    </head>
    <body>
        <div class="box">
        	<h1>OCORREU UM PROBLEMA</h1>
            <form name="formConsulta" action="edit">
                <fieldset>
               <p style="color:red">⚠️ Duplicidade encontrada. Esse documento já está vinculado a um paciente que solicitou o mesmo atendimento (Plano - especialidade).                             </p>
                    <br>
                    <center><p>Ao clicar, você será redirecionado para a tela inicial.</p></center>
                    <center><input class="botao1" type="submit" onclick="validar(event)"></input></center>
                </fieldset>
            </form>
        </div>
        <script src="scripts/validacao.js"></script>
    </body>
</html>